# Cache API

Provides a basic web resource caching service

## Running the application
After checking out the project, the service can be started using the following commands:
```
mvn clean compile
mvn exec:java
```

## API

| Method | Path                  | Description                                                                                              |
|--------|-----------------------|----------------------------------------------------------------------------------------------------------|
| GET    | /cache                | Get all (cache ids) in the cache                                                                         |
| GET    | /cache/{key}          | Get a specific resource from the cache (in the form of a json entity)                                    |
| GET    | /cache/{key}/original | Get a specific resource from the cache (in the form of the original response). See Note 1 for more info. |
| POST   | /cache                | Create a new request to add a resource to the cache                                                      |
| PUT    | /cache/{key}          | Manually update a resource in the cache                                                                  |
| DELETE | /cache/{key}          | Remove a specific resource from the cache                                                                |
| DELETE | /cache                | Remove all resources from the cache                 

## Notes
1. "What's up with `GET /cache/{key}/original`?" -- It wasn't clear from the instructions if the response from the api should contain the data, or literally be the original response. As this is a *service*, i'm going to have the default `GET cache/{key}` return json which a separate service could more easily utilize. However, (just to be complete) I'm also including this endpoint which reconstructs the original response..


## What's next (given more time)
- Unit/Integration tests (famous last words, i know)
- Add more sophistication to the service to allow things like process priority, TTL, metrics, authentication and authorization etc..
- Add api documentation via [swagger](http://swagger.io)  
- error handling to the RequestProcessor (what happens if a url 404's)? 