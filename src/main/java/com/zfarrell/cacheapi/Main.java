package com.zfarrell.cacheapi;

import com.zfarrell.cacheapi.db.CacheDAO;
import com.zfarrell.cacheapi.db.impl.MapBackedCacheDAO;
import com.zfarrell.cacheapi.service.CacheService;
import com.zfarrell.cacheapi.service.RequestProcessor;
import com.zfarrell.cacheapi.service.impl.CacheServiceImpl;
import com.zfarrell.cacheapi.service.impl.RequestProcessorImpl;
import org.eclipse.jetty.server.Server;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.io.IOException;
import java.net.URI;

/**
 * Main class.
 *
 */
public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);

    // Base URI server will listen on
    // todo: move to config
    public static final String BASE_URI = "http://localhost:8080/";

    /**
     * Starts HTTP server exposing JAX-RS resources defined in this application.
     * @return Jetty HTTP server.
     */
    public static Server startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in com.zfarrell package
        final ResourceConfig rc = new ResourceConfig();

        // setup bindings for hk2 to enable DI and lifecycle management
        rc.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(CacheServiceImpl.class).to(CacheService.class).in(Singleton.class);
                bind(MapBackedCacheDAO.class).to(CacheDAO.class).in(Singleton.class);
                bind(RequestProcessorImpl.class).to(RequestProcessor.class).in(Singleton.class);
            }
        });

        rc.packages("com.zfarrell");

        // create and start a new instance of jetty http server
        // exposing the Jersey application at BASE_URI
        return JettyHttpContainerFactory.createServer(URI.create(BASE_URI), rc);
    }

    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final Server server = startServer();
        logger.info("Jersey app started with WADL available at {}application.wadl", BASE_URI);
    }
}

