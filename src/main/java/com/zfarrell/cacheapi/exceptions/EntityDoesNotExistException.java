package com.zfarrell.cacheapi.exceptions;

/**
 * Marker exception thrown when an entity is requested but doesn't exist
 */
public class EntityDoesNotExistException extends RuntimeException {
    public EntityDoesNotExistException(String msg) {
        super(msg);
    }
}
