package com.zfarrell.cacheapi.dto;

import com.zfarrell.cacheapi.model.CachedObject;
import org.apache.http.Header;

/**
 * Created by zfarrell on 7/30/16.
 */
public class CacheResponseDTO extends AbstractModelDelegateDTO<CachedObject> {
    public CacheResponseDTO() {
        super(new CachedObject());
    }
    public CacheResponseDTO(CachedObject entity) {
        super(entity);
    }

    public String getId() {
        return entity.getId();
    }

    public void setId(String id) {
        entity.setId(id);
    }

    public Header[] getHeaders() {
        return entity.getResponseHeaders();
    }

    public void setHeaders(Header[] headers) {
        entity.setResponseHeaders(headers);
    }

    public String getBody() {
        return entity.getBody();
    }

    public void setBody(String body) {
        entity.setBody(body);
    }
}
