package com.zfarrell.cacheapi.dto;

import com.zfarrell.cacheapi.model.CacheRequest;

/**
 * Created by zfarrell on 7/30/16.
 */
public class CacheRequestDTO extends AbstractModelDelegateDTO<CacheRequest> {
    public CacheRequestDTO() {
        super(new CacheRequest());
    }

    public CacheRequestDTO(CacheRequest entity) {
        super(entity);
    }

    public String getUrl() {
        return entity.getUrl();
    }

    public void setUrl(String url) {
        entity.setUrl(url);
    }
}
