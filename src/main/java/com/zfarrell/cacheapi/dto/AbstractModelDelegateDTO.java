package com.zfarrell.cacheapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Implements delegate pattern, used for data transfer objects (DTO).
 *
 * DTO's are useful as it allows us to provide a consistent interface to the public api,
 *
 * while having the flexability to make changes to the internal model.
 */
abstract public class AbstractModelDelegateDTO<ModelType> {
    @JsonIgnore
    protected ModelType entity;

    public AbstractModelDelegateDTO(final ModelType entity) {
        this.entity = entity;
    }

    public ModelType toModel() {
        return entity;
    }
}
