package com.zfarrell.cacheapi.dto;

public class CacheIdDTO {
    private String id;

    public CacheIdDTO() {
    }

    public CacheIdDTO(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
