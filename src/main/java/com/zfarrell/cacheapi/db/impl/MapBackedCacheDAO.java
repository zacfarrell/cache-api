package com.zfarrell.cacheapi.db.impl;

import com.zfarrell.cacheapi.exceptions.EntityDoesNotExistException;
import com.zfarrell.cacheapi.db.CacheDAO;
import com.zfarrell.cacheapi.model.CachedObject;

import javax.inject.Singleton;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Cache "data access object" (DAO), using a Map as the data store.
 * Provides basic CRUD operations
 */
@Singleton
public class MapBackedCacheDAO implements CacheDAO {
    protected Map<String, CachedObject> cache;

    // index the urls so that we can determine if we're already caching a particular resource
    protected Map<String,String> urlsToCacheId = new ConcurrentHashMap<>();

    public MapBackedCacheDAO() {
        this(new ConcurrentHashMap<>());
    }

    public MapBackedCacheDAO(Map<String, CachedObject> cache) {
        this.cache = cache;
    }

    @Override
    public Optional<String> resolveIdFromUrl(String url) {
        return Optional.ofNullable( urlsToCacheId.get(url) );
    }

    @Override
    public boolean contains(String url) {
        return urlsToCacheId.containsKey(url);
    }

    @Override
    public Optional<CachedObject> get(String id) {
        return Optional.ofNullable(cache.get(id));
    }

    @Override
    public Set<CachedObject> getAll() {
        return cache.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toSet());
    }

    @Override
    public CachedObject add(CachedObject cachedObject) {
        if (null == cachedObject.getId()) {
            throw new IllegalArgumentException("Id cannot be null");
        }
        cache.put(cachedObject.getId(), cachedObject);
        urlsToCacheId.put(cachedObject.getUrl(), cachedObject.getId());
        return cachedObject;
    }

    @Override
    public CachedObject update(CachedObject cachedObject) {
        if (!cache.containsKey(cachedObject.getId())) {
            throw new EntityDoesNotExistException("no cache object with id '" + cachedObject.getId() + "'");
        }
        cache.put(cachedObject.getId(), cachedObject);
        return cachedObject;
    }

    @Override
    public CachedObject remove(String id) {
        // attempt to remove from cache
        // if it doesn't exist, we'll throw an EntityDoesNotExistException
        // if it does exist, we'll also remove from the urlsToCacheId map
        return Optional
                .ofNullable(cache.remove(id))
                .map((cachedObject -> {urlsToCacheId.remove(cachedObject.getUrl()); return cachedObject;}))
                .orElseThrow(() ->  new EntityDoesNotExistException("no cache object with id '" + id + "'"));
    }

    @Override
    public void removeAll() {
        cache = new ConcurrentHashMap<>();
    }
}
