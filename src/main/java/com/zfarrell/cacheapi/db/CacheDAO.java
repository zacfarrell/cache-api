package com.zfarrell.cacheapi.db;

import com.zfarrell.cacheapi.exceptions.EntityDoesNotExistException;
import com.zfarrell.cacheapi.model.CachedObject;

import java.util.Optional;
import java.util.Set;

/**
 * Handles data store access for the cache
 */
public interface CacheDAO {
    /**
     * @param url
     * @return the cache id for the previously cached object with the given url, or Optional.empty if the url hasn't been cached
     */
    Optional<String> resolveIdFromUrl(final String url);

    /**
     * @param url a web resource url
     * @return true if the url is currently in the cache; false if it is not
     */
    boolean contains(final String url);

    /**
     * @param id id of cached object to lookup
     *
     * @return the (optional) cached object if it exists, or Optional.none if it doesn't exist
     */
    Optional<CachedObject> get(final String id);

    /**
     * @return all cached objects
     */
    Set<CachedObject> getAll();

    /**
     * @param cachedObject the object to cache
     *
     * @throws IllegalArgumentException if the object doesn't have an ID
     *
     * @return the added cached object
     */
    CachedObject add(final CachedObject cachedObject);

    /**
     * @param cachedObject object to update
     *
     * @throws EntityDoesNotExistException if object doesn't already exist
     *
     * @return the updated object
     */
    CachedObject update(final CachedObject cachedObject);

    /**
     * Removes a single object from the cache
     * @param id id of object to remove
     * @return the object that was removed
     */
    CachedObject remove(final String id);

    /**
     * Removes all objects from cache.
     */
    void removeAll();
}
