package com.zfarrell.cacheapi.model;

/**
 * Represents a request to cache a web resource.
 *
 * For now this is basic and just takes the basic url of the resource we should cache.
 *
 * However it could be expanded to capture more advanced functionality, for example:
 *      1) request headers - include additional headers when making the request
 *      2) processing priority - based on the importance of the resource, we could have a priority queue for processing
 *      3) expiration - set a custom cache expiration for this object
 *      etc..
 */
public class CacheRequest {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
