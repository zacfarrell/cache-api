package com.zfarrell.cacheapi.model;

import org.apache.http.Header;

/**
 * Represents an object in the cache.
 */
public class CachedObject {
    /**
     * The cache id of this resource
     */
    private String id;

    /**
     * The original url of this resource
     */
    private String url;

    /**
     * headers returned from the resource
     */
    private Header[] responseHeaders;

    /**
     * body of the resource
     */
    private String body;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Header[] getResponseHeaders() {
        return responseHeaders;
    }

    public void setResponseHeaders(Header[] responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
