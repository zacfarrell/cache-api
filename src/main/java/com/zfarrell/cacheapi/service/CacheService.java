package com.zfarrell.cacheapi.service;

import com.zfarrell.cacheapi.exceptions.EntityDoesNotExistException;
import com.zfarrell.cacheapi.model.CachedObject;
import com.zfarrell.cacheapi.model.CacheRequest;

import java.util.Set;

/**
 * Primary mechanism for operating on the cache. Provides expected CRUD functionality.
 */
public interface CacheService {
    /**
     * Takes a cache request, queues it for processing, and returns the cache id.
     *
     * The Cache ID can be used to retrieve the cached object at a future time
     *
     * @param request the request to cache an entity at some url
     * @return cache id
     */
    String add(CacheRequest request);

    /**
     * Update an existing object in the cache
     *
     * @param response object to update
     *
     * @throws EntityDoesNotExistException if object doesn't already exist
     *
     * @return the updated object
     */
    CachedObject update(final CachedObject response);

    /**
     * Retrieve an object from the cache
     *
     * @throws EntityDoesNotExistException if object does not exist
     *
     * @param id id of object
     *
     * @return The object from the cache
     */
    CachedObject get(final String id);

    /**
     * @return all objects from the cache
     */
    Set<CachedObject> getAll();

    /**
     * Removes a single object from the cache
     * @param id id of object to remove
     * @return the object that was removed
     */
    CachedObject remove(final String id);

    /**
     * Removes all objects from the cache
     */
    void removeAll();
}
