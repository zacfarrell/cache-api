package com.zfarrell.cacheapi.service.impl;

import com.zfarrell.cacheapi.exceptions.EntityDoesNotExistException;
import com.zfarrell.cacheapi.model.CacheRequest;
import com.zfarrell.cacheapi.model.CachedObject;
import com.zfarrell.cacheapi.service.CacheService;
import com.zfarrell.cacheapi.service.RequestProcessor;
import com.zfarrell.cacheapi.db.CacheDAO;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Set;

/**
 * Created by zfarrell on 7/30/16.
 */
@Singleton
public class CacheServiceImpl implements CacheService {
    @Inject
    protected CacheDAO cacheDAO;

    @Inject
    private RequestProcessor requestProcessor;

    @Override
    public String add(CacheRequest request) {
        return requestProcessor.queue(request);
    }

    @Override
    public CachedObject update(CachedObject response) {
        return cacheDAO.update(response);
    }

    @Override
    public CachedObject get(String id) {
        return cacheDAO.get(id).orElseThrow(() -> new EntityDoesNotExistException("no entity with id '" + id + "'"));
    }

    @Override
    public Set<CachedObject> getAll() {
        return cacheDAO.getAll();
    }

    @Override
    public CachedObject remove(String id) {
        return cacheDAO.remove(id);
    }

    @Override
    public void removeAll() {
        cacheDAO.removeAll();
    }
}
