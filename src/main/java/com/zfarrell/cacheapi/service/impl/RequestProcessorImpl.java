package com.zfarrell.cacheapi.service.impl;

import com.zfarrell.cacheapi.db.CacheDAO;
import com.zfarrell.cacheapi.model.CacheRequest;
import com.zfarrell.cacheapi.model.CachedObject;
import com.zfarrell.cacheapi.service.RequestProcessor;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


import javax.inject.Inject;
import javax.inject.Singleton;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of the RequestProcessor interface.
 *
 * We use an executor service so that we can process web requests without blocking the main web service thread
 * when a thread is available, it makes the web request and then stores then passes the results to the CacheDAO
 *
 * todo: error handling
 */
@Singleton
public class RequestProcessorImpl implements RequestProcessor {
    private static Logger logger = LoggerFactory.getLogger(RequestProcessorImpl.class);
    protected ExecutorService executorService;

    @Inject
    protected CacheDAO cacheDAO;

    public RequestProcessorImpl() {
        // initially let's use a cached thread pool as we expect infrequent
        this(Executors.newCachedThreadPool());
    }

    public RequestProcessorImpl(ExecutorService executorService) {
        this.executorService = executorService;
    }

    @Override
    public String queue(CacheRequest request) {
        Optional<String> id = cacheDAO.resolveIdFromUrl(request.getUrl());

        if (!id.isPresent()) { // url isn't currently in the cache

            // cheap unique id
            id = Optional.of(UUID.randomUUID().toString());

            logger.debug("Cache miss: queuing request for '{}' with id '{}'", request.getUrl(), id.get());


            // queue http request worker
            executorService.submit(new WebResourceWorker(id.get(), request));

        } else { // url is already cached
            logger.debug("Cache hit for url '{}'. ID: '{}'", request.getUrl(), id.get());
        }

        return id.get();
    }

    /**
     * Worker which actually does the work of making the http request, and saving the result with the cache DAO
     */
    private class WebResourceWorker implements Runnable {
        private CacheRequest request;
        private String id;
        private CloseableHttpClient httpClient;

        public WebResourceWorker(String id, CacheRequest request) {
            this.request = request;
            this.id = id;
            httpClient = HttpClients.createDefault();
        }

        @Override
        public void run() {
            // 1) make request
            HttpGet httpGet = new HttpGet(request.getUrl());
            CloseableHttpResponse response = null;
            String responseBody = null;
            Header[] headers = null;
            try {
                response = httpClient.execute(httpGet);

                headers = response.getAllHeaders();
                responseBody = EntityUtils.toString(response.getEntity());
            } catch (Exception e) {
                logger.error("Unable to get resource", e);
            } finally {
                try {
                    response.close();
                } catch (IOException e) {
                    String errorMsg = String.format("error closing http client for request [%s]: %s", id, request.toString());
                    throw new RuntimeException(errorMsg, e);
                }
            }


            // 2) build cached object
            CachedObject obj = new CachedObject();
            obj.setId(id);
            obj.setBody(responseBody);
            obj.setResponseHeaders(headers);
            obj.setUrl(request.getUrl());


            // 3) add object to cacheDao
            cacheDAO.add(obj);
        }
    }
}
