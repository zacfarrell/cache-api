package com.zfarrell.cacheapi.service;

import com.zfarrell.cacheapi.model.CacheRequest;

/**
 * Service responsible for creating cache object id, making the http request, and caching the results.
 */
public interface RequestProcessor {
    /**
     * Adds a request to the queue
     *
     * @param request to queue
     *
     * @return the id of the cached object (as it will be stored in the cache)
     */
    String queue(final CacheRequest request);
}
