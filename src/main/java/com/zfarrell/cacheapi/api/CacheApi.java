package com.zfarrell.cacheapi.api;

import com.zfarrell.cacheapi.dto.CacheIdDTO;
import com.zfarrell.cacheapi.dto.CacheRequestDTO;
import com.zfarrell.cacheapi.dto.CacheResponseDTO;
import com.zfarrell.cacheapi.model.CachedObject;
import com.zfarrell.cacheapi.service.CacheService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by zfarrell on 7/30/16.
 */
@Path("/cache")
@Produces(MediaType.APPLICATION_JSON)
public class CacheApi {
    @Inject
    private CacheService cacheService;

    @POST
    public CacheIdDTO addToCache(final CacheRequestDTO requestDTO) {
        return new CacheIdDTO( cacheService.add(requestDTO.toModel()) );
    }

    @PUT
    public CacheResponseDTO update(final CacheResponseDTO responseDTO) {
        return new CacheResponseDTO( cacheService.update(responseDTO.toModel()) );
    }

    /**
     * @return all of the cached object's IDs. returning all of the full objects could turn into huge responses
     */
    @GET
    public Set<CacheIdDTO> getAll() {
        return cacheService.getAll().stream().map(cachedObject -> new CacheIdDTO(cachedObject.getId())).collect(Collectors.toSet());
    }

    @GET
    @Path("/{id}")
    public CacheResponseDTO getFromCache(final @PathParam("id") String id) {
        return new CacheResponseDTO(cacheService.get(id));
    }

    /**
     * NOTE:
     * It wasn't clear from the instructions if the response from the api should contain the data, or literally be the original response.
     *
     * As this is a *service*, i'm going to have the default GET cache/{key} return json which a separate service could more easily utilize.
     *
     * However, (just to be complete) I'm also including this endpoint which reconstructs the original response..
     */
    @GET
    @Path("/{id}/original")
    public Response getFromCacheAndReconstructOriginal(final @PathParam("id") String id) {
        CachedObject object = cacheService.get(id);
        Response.ResponseBuilder responseBuilder = Response.ok(object.getBody());

        // add all of the headers to the response
        Arrays.stream(object.getResponseHeaders()).forEach( header ->
            responseBuilder.header(header.getName(), header.getValue())
        );

        return responseBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    public CacheResponseDTO removeFromCache(final @PathParam("id") String id) {
        return new CacheResponseDTO(cacheService.remove(id));
    }

    @DELETE
    public void removeAll() {
        cacheService.removeAll();
    }
}
