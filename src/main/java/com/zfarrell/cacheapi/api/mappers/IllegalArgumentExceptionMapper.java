package com.zfarrell.cacheapi.api.mappers;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.HashMap;
import java.util.Map;

/**
 * Catches the IllegalArgument Exception, and build a 400 response to return
 */
@Provider
public class IllegalArgumentExceptionMapper implements ExceptionMapper<IllegalArgumentException> {
    @Override
    public Response toResponse(IllegalArgumentException e) {
        final String msg = e.getMessage();
        Map<String, String> err = new HashMap<>();
        err.put("error", msg);
        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(err)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
