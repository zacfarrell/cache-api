package com.zfarrell.cacheapi.api.mappers;

import com.zfarrell.cacheapi.exceptions.EntityDoesNotExistException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.HashMap;
import java.util.Map;

/**
 * Catches the EntityDoesNotExist Exception, and build a 404 response to return
 */
@Provider
public class EntityDoesNotExistExceptionMapper implements ExceptionMapper<EntityDoesNotExistException> {
    @Override
    public Response toResponse(EntityDoesNotExistException e) {
        final String msg = e.getMessage();
        Map<String, String> err = new HashMap<>();
        err.put("error", msg);
        return Response
                .status(Response.Status.NOT_FOUND)
                .entity(err)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
